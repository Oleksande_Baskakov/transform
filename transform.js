const { Transform } = require('stream');

class TransformNumberStream extends Transform {
    #buffer = [];
    _transform(chunk, encoding, cb) {
      let arr = [];
      const lines = chunk.toString().split('\n');

      if (this.#buffer[this.#buffer.length - 1] && this.#buffer[this.#buffer.length - 1].length !== 30) {
        this.#buffer = this.#buffer.filter(el => el !== '');
        const concat = this.#buffer.pop();

        const firstEl = lines[0] + concat;
        lines[0] = firstEl;
        arr = [...this.#buffer, ...lines];
      } else {
        const arrOfStrings = chunk
          .toString()
          .split('\n')
          .filter(el => el !== '');

        this.#buffer = this.#buffer.filter(el => el !== '');
        arr = [...this.#buffer, ...arrOfStrings];
      }

      this.#buffer = [];

      if (arr.length % 3 || arr[arr.length - 1].length !== 30) {
        const marker = arr.length % 3 || 3;
        this.#buffer = arr.slice(-marker);
        arr.length -= (marker);
      }

      let result = '';
      arr = arr.filter(el => el !== '');

      for (let n = 0; n < arr.length - 2; n += 3) {
        let tmp = '';

        for (let i = 0; i < 30; i += 3) {
          const number = `${arr[n][i]}${arr[n][i + 1]}${arr[n][i + 2]}\n${arr[n + 1][i]}${arr[n + 1][i + 1]}${arr[n + 1][i + 2]}\n${arr[n + 2][i]}${arr[n + 2][i + 1]}${arr[n + 2][i + 2]}`;

          switch (number) {
          case ' _ \n|_|\n _|': tmp += 9;
            break;
          case ' _ \n|_|\n|_|': tmp += 8;
            break;
          case ' _ \n| |\n  |': tmp += 7;
            break;
          case ' _ \n|_ \n|_|': tmp += 6;
            break;
          case ' _ \n|_ \n _|': tmp += 5;
            break;
          case '   \n|_|\n  |': tmp += 4;
            break;
          case ' _ \n _|\n _|': tmp += 3;
            break;
          case ' _ \n _|\n|_ ': tmp += 2;
            break;
          case '   \n  |\n  |': tmp += 1;
            break;
          case ' _ \n| |\n|_|': tmp += 0;
            break;
          default: tmp += '?';
            break;
          }

          if (i + 3 === 30) {
            result += `${tmp}\n`;
          }
        }
      }

      // console.log(result)
      // console.log(arr[1][0],arr[1][1], arr[1][2]);
      // console.log(arr[2][0],arr[2][1], arr[2][2]);

      this.push(result);
      cb();
    }
}

module.exports = TransformNumberStream;
