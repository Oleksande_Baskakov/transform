const fs = require('fs');
const file = fs.createWriteStream('data.txt');

const SIZE = process.argv[2] || 100; // Mb

// eslint-disable-next-line no-sync
const data = fs.readFileSync('./utils/row-data.txt');

for (let i = 0; i < 350 * SIZE; i++) {
  file.write(data);
}

file.end();
